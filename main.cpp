#include <time.h>
#include <iostream>
#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <string>
using namespace std;

void SetCursorPosition(int XPos, int YPos);
void getCursor(int* x, int* y);
void drawboat(int *h,int *i);
void drawvoid();
int A(int *i,int *h);
int D(int *i,int *h);
int W(int *i,int *h);
int S(int *i,int *h);
int E(int *i,int *h);
int Q(int *i,int *h);
void bulletto(int bli[5],int blh[5]);
void interface(int lifepoint,int score,int barrier);
void interfacecolor();
void drawTUBE(int x ,int y,int size);
int hitWall(int xboat,int yboat,int xwall,int ywall,int size);
void endscene(int score);
int scorewall(int xboat,int yboat,int xwall,int ywall,int wallenable,int size);
int menu();
int game();
void showScores();
void drawFrame();
void drawbarrier(int *h,int *i);
#define key_up 256+72
#define key_down 256+80
#define key_right 256+75
#define key_left 256+77
static int get_code(void);
#define paMaxX 237
#define paMaxY 53
#define maxlife 10
#define esc 0x1B

int main () {//237*57
    printf("\e[?25l");//ปิดcursor
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    sleep(2);//รอโปรแกรมเปิด
    while(1){
        int state =menu();
        if(state==0){
            endscene(game());
        }
        else if(state==1){
            showScores();
        }
        else if(state==2){
            return(0);
        }
    }
}

int game(){
    clock_t start_tbul,this_tbul,start_program,this_program,start_wall,this_wall,start_eneblewall,this_eneblewall,start_eneblebar,this_eneblebar;
    start_program=clock();
    start_tbul=start_program;
    start_wall=start_program;
    start_eneblewall=start_program;
    start_eneblebar=start_program;

    int i=115,h=50,*ip,*hp,bli[5]={-1,-1,-1,-1,-1},blh[5]={-1,-1,-1,-1,-1},lasttikwall=9,score=0,speed=1,goal=2;
    char lastinput;
    int shoot=0,j,temp,ft=0,lifepoint=maxlife,numwall=0,breaker=0;

    int numbarrier=3,eneblebar=0;

    ip=&i;
    hp=&h;

    double thistimeMillisecbullet;
    //enemy
    int star[2][20];//[0][20]=x ,[1][20]=y;
    int wall[5][10];//0,..=x : 1,..=y : 2,..=size 3,eneble/disable 4,M O D E
    int wallitem[10];
    srand(time(NULL));
    for(int i=0;i<20;i++){//randomค่าเริ่มแรก
        star[0][i]=rand()%5+1;//2-5
        star[1][i]=rand()%180+51;//150-300
    }
    for(int i=0;i<10;i++){
        wall[0][i]=rand()%169+51;//x 220-51
        wall[1][i]=0;//y
        wall[2][i]=rand()%3+4;//size
        wall[3][i]=0;
        //wall[4][i]
    }

    int inp;
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    sleep(1);//รอโปรแกรมเปิด

    start_color();
    init_pair(1,COLOR_BLUE, COLOR_RED);
    wbkgd(stdscr, COLOR_PAIR(1));
    printf("\e[?25l");//ปิดcursor
    SetCursorPosition(i,h);


    while(1){
        this_program=clock();
        this_tbul=this_program;
        this_wall=this_program;
        this_eneblewall=this_program;
        this_eneblebar=this_program;

        SetCursorPosition(i,h);
        inp=get_code();//รับจากแป้นพิมพ์

        //ระเบิด
        /*for(int i=0;i<10;i++){
            while(1){
                printf("hi :");
                break;
            }
        }*/
        if(inp=='a'||inp==key_left){
            A(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='a';
        }
        else if(inp=='d'||inp==key_right){
            D(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='d';
        }
        else if(inp=='w'||inp==key_up){
            W(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='w';
        }
        else if(inp=='s'||inp==key_down){
            S(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='s';
        }
        else if(inp=='x'||inp==esc){
            return score;
        }
        else if(inp=='b'){
            if(eneblebar==1){
                drawbarrier(ip,hp);
            }
            lastinput='b';
        }
        else if(inp=='e'&&eneblebar!=1){
            E(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='d';
        }
        else if(inp=='q'&&eneblebar!=1){
            Q(ip,hp);
            if(eneblebar==1){
                drawbarrier(ip,hp);

            }
            drawboat(ip,hp);
            lastinput='a';
        }
        else if(inp==' '){//ยิง
            if(numbarrier>0){
                numbarrier--;
                eneblebar=1;
            }
            /*if(shoot>=3){//ยิงได้นัดวนๆ//จำนวนการยิง  (ยิงได้สูงสุด5แก้เลข here!!)
                shoot=0;
            }
            if(blh[shoot]==-1){//ยิง
                bli[shoot]=i+3;
                blh[shoot]=h-1;
                if(blh[shoot]>0){
                    SetCursorPosition(bli[shoot],blh[shoot]);
                    printf("\033[41m\033[1;33m|\033[0m");
                    shoot++;
                }

            }*/
        }
        if((this_eneblebar-start_eneblebar)/CLOCKS_PER_SEC==3){
            eneblebar=0;
            SetCursorPosition(i-1,h-1);
            printf("\033[41;1;34m         ");
            SetCursorPosition(i-1,h);
            printf("\033[41;1;34m         ");
            SetCursorPosition(i-1,h+1);
            printf("\033[41;1;34m         ");
            start_eneblebar=this_eneblebar;
        }


        thistimeMillisecbullet=(double)(this_tbul-start_tbul)/(double)CLOCKS_PER_SEC*30;
        if(((int)thistimeMillisecbullet%10)==1){//วนรูปทุก 50 มิลลิเซค
            bulletto(bli,blh);
            start_tbul=this_tbul;


            if(lastinput=='a'){
                A(ip,hp);
                if(eneblebar==1){
                    drawbarrier(ip,hp);
                }
                drawboat(ip,hp);
                lastinput='a';
            }
            else if(lastinput=='d'){
                D(ip,hp);
                if(eneblebar==1){
                    drawbarrier(ip,hp);

                }
                drawboat(ip,hp);
                lastinput='d';
            }
            else if(lastinput=='w'){
                W(ip,hp);
                if(eneblebar==1){
                    drawbarrier(ip,hp);

                }
                drawboat(ip,hp);
                lastinput='w';
            }
            else if(lastinput=='s'){
                S(ip,hp);
                if(eneblebar==1){
                    drawbarrier(ip,hp);

                }
                drawboat(ip,hp);
                lastinput='s';
            }
        }

        //star
        /*for(int a=0;a<20;a++){
            if(star[0][a]!=-1&&star[1][a]!=-1){
                SetCursorPosition(star[1][a],star[0][a]);
                printf("\033[41m\033[1;35m*\033[0m");
            }

            for(int j=0;j<5;j++){
                if(star[1][a]==bli[j]&&blh[j]==star[0][a]){//ยิงโดนดาว
                    system("((speaker-test -t sine -f 700)& pid=$!; sleep 0.1s; kill -9 $pid) > /dev/null"); //เสียงbeep
                    SetCursorPosition(star[1][a],star[0][a]);
                    printf("\033[41m \033[0m");
                    star[1][a]=-1;
                    //star[0][a]=-1;
                    bli[j]=-1;
                    blh[j]=-1;
                    lifepoint--;
                }
            }
        }*/

        //score time
        for(int check=0;check<10;check++){
            if(wall[3][check]==1){
                wall[3][check]=scorewall(i,h,wall[0][check],wall[1][check],wall[3][check],wall[2][check]);
                if(wall[3][check]==2){
                    score++;
                    if(check%3==2){
                        numbarrier++;
                    }
                }
            }

        }

        if(lifepoint<=0){
            break;
        }
        if(score>=goal&&(speed<6)){
            speed++;
            goal=speed;
        }//2 4 8 16
        interface(lifepoint,score,numbarrier);
        thistimeMillisecbullet=((double)(this_wall-start_wall)/(double)CLOCKS_PER_SEC)*(speed*5);//*5    //บัคคะแนนถึงประมาณ20แล้วกำแพงออกมาพร้อมกัน10กำแพงทีเดียว
        if((int)thistimeMillisecbullet%10>=1){//ขยับกำแพงทุก1วิ
            for(int check=0;check<10;check++){
                if(wall[3][check]>=1){
                    drawTUBE(wall[0][check],wall[1][check],wall[2][check]);
                    wall[1][check]++;
                    if(check%3==2){
                        if((wall[1][check]-4>0)&&(wall[1][check]-4<paMaxY)){
                            SetCursorPosition(wall[0][check],wall[1][check]-3);
                            printf("\033[41m \033[0m");
                            SetCursorPosition(wall[0][check]+(wall[2][check]*4),wall[1][check]-3);
                            printf("\033[41m \033[0m");
                            SetCursorPosition(wall[0][check],wall[1][check]-2);
                            printf("\033[41mx\033[0m");
                            SetCursorPosition(wall[0][check]+(wall[2][check]*4),wall[1][check]-2);
                            printf("\033[41mx\033[0m");
                        }
                        if(wall[1][check]-4==paMaxY){
                            SetCursorPosition(wall[0][check],wall[1][check]-3);
                            printf("\033[41m \033[0m");
                            SetCursorPosition(wall[0][check]+(wall[2][check]*4),wall[1][check]-3);
                            printf("\033[41m \033[0m");
                        }

                    }
                }
                if(wall[1][check]>59){
                    wall[3][check]=0;
                    wall[0][check]=rand()%169+51;//x 220-51
                    wall[1][check]=0;//y
                    wall[2][check]=rand()%3+4;//size

                }
                //SetCursorPosition(0,check);
                //printf("%d %d %d %d",wall[3][check],wall[0][check],wall[1][check],wall[2][check]);
            }
            breaker=0;
            start_wall=this_wall;
        }

        if((this_eneblewall-start_eneblewall)/CLOCKS_PER_SEC>=((100-(speed*15))/10)){//5        //บัคเมื่อแต้ม 20 numwal lม่ขยับ !!
            if(numwall>9){
                numwall=0;
            }
            wall[3][numwall]=1;
            numwall++;
            start_eneblewall=this_eneblewall;
            //SetCursorPosition(0,10);
            //printf("%d",numwall);
        }

        if(breaker!=1){//ดีเลย์ชนกำแพง
            for(int check=0;check<10;check++){
                if((hitWall(i,h,wall[0][check],wall[1][check],wall[2][check])==1)&&eneblebar==0){
                    lifepoint--;
                    breaker=1;
                }
            }
        }
        if(ft<5){//สร้างเรือรอบแรกสุด
            wall[3][0]=1;
            numwall=1;
            ft++;
            for(int i=0;i<57;i++){
                for(int j=0;j<237;j++){
                    SetCursorPosition(j,i);
                    printf("\033[41m \033[0m");
                }
            }
            interfacecolor();
            SetCursorPosition(i,h);
            drawboat(ip,hp);
        }
        SetCursorPosition(0,0);
        //printf("%d",wall[1][0]);
    }
    return score;
}

static int get_code(void){
    int ch = getch();

    if (ch == 0 || ch == 224)
        ch = 256 + getch();

    return ch;
}
void SetCursorPosition(int XPos, int YPos) {
    printf("\033[%d;%dH",YPos+1,XPos+1);
}
void getCursor(int* x, int* y) {
   printf("\033[6n");  /* This escape sequence !writes! the current
                          coordinates to the terminal.
                          We then have to read it from there, see [4,5].
                          Needs <termios.h>,<unistd.h> and some others */
   scanf("\033[%d;%dR", x, y);
}
void drawboat(int *h,int *i){
    SetCursorPosition(*h,*i);
    printf("\033[41m\033[1;32m <-Y-> \033[0m");
}
void drawvoid(){
    printf("\033[41m       ");
}
void drawbarrier(int *h,int *i){
    SetCursorPosition(*h-1,*i-2);
    printf("\033[41;1;34m         ");
    SetCursorPosition(*h-1,*i-1);
    printf("\033[41;1;34m ╔═════╗ ");
    SetCursorPosition(*h-1,*i);
    printf("\033[41;1;34m ║     ║ ");
    SetCursorPosition(*h-1,*i+1);
    printf("\033[41;1;34m ╚═════╝ ");
    SetCursorPosition(*h-1,*i+2);
    printf("\033[41;1;34m         ");
}

int A(int *i,int *h){
    if(*i>51){
            SetCursorPosition(--*i,*h);
            }
            return 0;
}
int D(int *i,int *h){
    if(*i<229){
         SetCursorPosition(++*i,*h);
    }
    return 0;
}
int E(int *i,int *h){
    SetCursorPosition(*i,*h);
    drawvoid();
    if(*i<229){
        *i+=15;
        if(*i>230){
            *i=230;
        }
        SetCursorPosition(*i,*h);
    }
    return 0;
}
int Q(int *i,int *h){
    SetCursorPosition(*i,*h);
    drawvoid();
    if(*i>51){
        *i-=15;
        if(*i<50){
            *i=50;
        }
        SetCursorPosition(*i,*h);
    }
    return 0;
}
int W(int *i,int *h){
    if(*h>1){
            SetCursorPosition(*i,*h);
            drawvoid();
            SetCursorPosition(*i,--*h);
            }
            return 0;
}
int S(int *i,int *h){
    if(*h<55){
        SetCursorPosition(*i,*h);
        drawvoid();
        SetCursorPosition(*i,++*h);
    }
    return 0;
}
void bulletto(int bli[5],int blh[5]){
    for(int j=0;j<5;j++){//เช็คกระสุน
                if(blh[j]!=-1){
                    if(blh[j]==1){//ยิงออกนอกจอ
                        SetCursorPosition(bli[j],blh[j]);
                        printf("\033[41m ");
                        blh[j]=-1;
                        bli[j]=-1;
                    }
                    else if(blh[j]>0){//กระสุนเคลื่อนที่
                        SetCursorPosition(bli[j],blh[j]);
                        printf("\033[41m ");
                        SetCursorPosition(bli[j],--blh[j]);
                        printf("\033[41m\033[1;33m|\033[0m");
                    }
                }
            }
}
void interface(int lifepoint,int score ,int barrier){
    //heathbar
    SetCursorPosition(20,13);
    printf("\033[40m\033[1;38mLIFEPOINT");
    SetCursorPosition(20,14);
    printf("\033[40m\033[1;36m------------");
    SetCursorPosition(20,15);
    printf("\033[40m|\033[0m");
    SetCursorPosition(31,15);
    printf("\033[40m|\033[0m");
    SetCursorPosition(20,16);
    printf("\033[40m\033[1;36m------------");
    for(int i=1;i<=lifepoint;i++){
        SetCursorPosition(20+i,15);
        printf("\033[41m\033[1;36m \033[0m");
    }
    for(int i=1;i<=maxlife-lifepoint;i++){
        SetCursorPosition(20+lifepoint+i,15);
        printf("\033[40m\033[1;36m \033[0m");
    }

    //score
    SetCursorPosition(20,9);
    printf("\033[40m\033[1;38mSCORE : %d",score);

    SetCursorPosition(5,20);
    printf("\033[0;1;34m ╔═════╗ ");
    SetCursorPosition(5,21);
    printf("\033[0;1;34m         ");
    SetCursorPosition(5,22);
    printf("\033[0;1;34m ╚═════╝ ");

    SetCursorPosition(14,21);
    printf(": %d  press 'spacebar to enable'",barrier);


    SetCursorPosition(5,44);
    printf("\033[40m\033[1;38mQ ,E   DASH FOR LIFE!!");
    SetCursorPosition(5,47);
    printf("\033[40m\033[1;38m  w  ");
    SetCursorPosition(5,48);
    printf("\033[40m\033[1;38ma s d to Move.");
    SetCursorPosition(8,51);
    printf("\033[40m\033[1;38m b to Stand Still.");
    SetCursorPosition(3,53);
    printf("\033[40m\033[1;38mWarning!:when sheid is up you can't use Dash");


}
void interfacecolor(){
    for(int i=0;i<57;i++){
        for(int j=0;j<237;j++){
            if(i==0||i==56){//กรอบ
                SetCursorPosition(j,i);
                printf("\033[42m\033[1;36m-\033[0m");
            }
            else{
                SetCursorPosition(0,i);
                printf("\033[42m\033[1;36m|\033[0m");
                SetCursorPosition(50,i);
                printf("\033[42m\033[1;36m|\033[0m");
                SetCursorPosition(237,i);
                printf("\033[42m\033[1;36m|\033[0m");
            }//กรอบจบ

            //สีinterface
            if(i>0&&i<56){
                if(j>0&&j<50){
                    SetCursorPosition(j,i);
                    printf("\033[40m \033[0m");
                }
            }
        }
    }
}
void drawTUBE(int x ,int y,int size){
    for(int tall=0;tall<4;tall++){
       for(int i=51;i<236;i++){
           if((y-tall>0)&&(y-tall<56)){
                if((i<=x||i>=x+(size*4))&&tall!=3){
                    SetCursorPosition(i,y-tall);
                    printf("\033[41m\033[1;35m#\033[0m");
                }
                else if(tall==3&&(i<=x||i>=x+(size*4))){
                    SetCursorPosition(i,y-tall);
                    printf("\033[41m\033[1;35m \033[0m");
                }
            }
        }
    }
}
int hitWall(int xboat,int yboat,int xwall,int ywall,int size){
    int value=0;
    if((ywall==yboat||ywall-1==yboat)||ywall-1==yboat){
        if((xboat+1<=xwall||xboat+1>=xwall+(size*4))||(xboat+6<=xwall||xboat+6>=xwall+(size*4))){
            //SetCursorPosition(0,0);
            //printf("hi");
            value=1;
        }
    }
    return value;
}
int scorewall(int xboat,int yboat,int xwall,int ywall,int wallenable,int size){
    if(wallenable<=1){
        if(ywall==yboat){
            if((xboat+1>xwall)&&(xboat+6<xwall+(size*4))){
                wallenable++;
            }
        }
    }
    return wallenable;
}

int menu(){
    printf("\e[?25l");
    char inp;
    int currentstate=0;
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    int first=0;

    while(1){
        usleep(5000);
        if(first<3){
            for(int i=0;i<57;i++){
                for(int j=0;j<237;j++){
                    SetCursorPosition(j,i);
                    printf("\033[100m \033[0m");
                }
            }
            first++;
SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-10);
printf("\033[100;1;91m  _|_|_|    _|    _|_|_|_|_|    _|      _|_|_|                                          _|  _|                    _|  _|_|_|                                                  _|  ");
SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-9);
printf("_|        _|  _|      _|      _|  _|  _|        _|_|_|      _|_|_|    _|_|_|    _|_|    _|  _|    _|_|_|      _|_|_|  _|    _|    _|_|    _|    _|    _|_|    _|_|_|      _|_|_|  ");SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-8);
printf("_|  _|_|  _|  _|      _|      _|  _|    _|_|    _|    _|  _|    _|  _|        _|_|_|_|  _|_|_|_|  _|    _|  _|    _|  _|_|_|    _|_|_|_|  _|    _|  _|    _|  _|    _|  _|    _|  ");SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-7);
printf("_|    _|  _|  _|      _|      _|  _|        _|  _|    _|  _|    _|  _|        _|            _|    _|    _|  _|    _|  _|    _|  _|        _|    _|  _|    _|  _|    _|  _|    _|  ");SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-6);
printf("  _|_|_|    _|        _|        _|    _|_|_|    _|_|_|      _|_|_|    _|_|_|    _|_|_|      _|    _|    _|    _|_|_|  _|_|_|      _|_|_|    _|_|_|    _|_|    _|    _|    _|_|_|  ");SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-5);
printf("                                                _|                                                                                              _|                                ");SetCursorPosition((paMaxX/2)-78,(paMaxY/2)-4);
printf("                                                _|                                                                                          _|_|                                  ");
        SetCursorPosition(paMaxX-50,paMaxY-6);
        printf("\033[97mPress W to Move Up");
        SetCursorPosition(paMaxX-50,paMaxY-4);
        printf("\033[97mPress S to Move Down");
        SetCursorPosition(paMaxX-50,paMaxY-2);
        printf("\033[97mPress spacebar to select");

        SetCursorPosition((paMaxX/2)-78,paMaxY-2);
        printf("\033[97mMade by Weerapat Inudom 64010823");
        }
        //SetCursorPosition(0,0);
        //printf("\e[0m%d",currentstate);
        inp=getch();
        if(inp=='w'){
            if(currentstate>0)
                currentstate--;
        }
        else if(inp=='s'){
            if(currentstate<2)
                currentstate++;
        }
        else if(inp==' '){
            return currentstate;
        }
        if(currentstate==0){
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+6);
            printf("\033[107m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+7);
            printf("\033[107;31m     Play     \033[0m");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+8);
            printf("\033[107m              ");
        }
        //PlayButton
        else{
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+6);
            printf("\033[104m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+7);
            printf("\033[104m     Play     ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+8);
            printf("\033[104m              ");
        }
        //LeaderboardsHighlighted
        if(currentstate==1){
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+12);
            printf("\033[107m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+13);
            printf("\033[107;31m    Scores    \033[0m");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+14);
            printf("\033[107m              ");
        }
        //LeaderboardsButton
        else{
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+12);
            printf("\033[104m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+13);
            printf("\033[104m    Scores    ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+14);
            printf("\033[104m              ");
        }
        //ExitHighlighted
        if(currentstate==2){
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+18);
            printf("\033[107m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+19);
            printf("\033[107;31m     Exit     \033[0m");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+20);
            printf("\033[107m              ");
        }
        //ExitButton
        else{
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+18);
            printf("\033[104m              ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+19);
            printf("\033[104m     Exit     ");
            SetCursorPosition((paMaxX/2)-8,(paMaxY/2)+20);
            printf("\033[104m              ");
        }
    }

}

void endscene(int score){
    endwin();
    for(int i=0;i<57;i++){
        for(int j=0;j<237;j++){
            SetCursorPosition(j,i);
            printf("\e[40m ");
        }
    }

    for(int x=17;x<=228;x++){
        for(int y=4;y<24;y++){
            //G
            if(x>=17&&x<=40){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=20&&y<=23){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=17&&x<=21){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=29&&x<=40){// -
                if(y>=12&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=36&&x<=40){
                if(y>=12&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //A
            if(x>=43&&x<=66){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=12&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if((x>=43&&x<=47)||(x>=62&&x<=66)){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //M
            if((x>=69&&x<=78)||(x>=83&&x<=92)){
                if((y>=4&&y<=7)){// _ _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=79&&x<=82){//  |กลาง
                if(y>=8&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if((x>=69&&x<=73)||(x>=88&&x<=92)){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //E x+26
            if(x>=95&&x<=99){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=95&&x<=118){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=12&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=20&&y<=23){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //O
            if((x>=123&&x<=127)||(x>=142&&x<=146)){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=123&&x<=146){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=20&&y<=23){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //v x+26
            if((x>=149&&x<=153)||(x>=168&&x<=172)){//  |
                if(y>=4&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if((x>=154&&x<=158)||(x>=163&&x<=167)){//  |
                if(y>=16&&y<=19){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=159&&x<=162){//  |
                if(y>=20&&y<=23){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //E x+80  175
            if(x>=175&&x<=179){//  |
                if(y>=4&&y<24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=175&&x<=198){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=12&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=20&&y<=23){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }

            //R 172+52
            if(x>=201&&x<=205){//  |
                if(y>=4&&y<=24){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=224&&x<=228){//  |
                if((y>=4&&y<15)||(y>=20&&y<=23)){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=219&&x<=223){//  |
                if(y>=16&&y<=19){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
            if(x>=201&&x<=228){
                if(y>=4&&y<=7){// _
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
                if(y>=12&&y<=15){
                    SetCursorPosition(x,y);
                    printf("\e[41m ");
                }
            }
        }
    }

    SetCursorPosition(95,38);
    printf("\e[0;1;96mScore :  %d\e[0;1;96m",score);

    SetCursorPosition(95,41);
    printf("\e[0;4;1;96mEnter Your Name :\e[0;1;34m");

    FILE *cmd=popen("echo -n $HOME", "r");
    char result[100]={0x0};
    fgets(result, sizeof(result), cmd);
    pclose(cmd);
    string fullpath=result;
    fullpath+="/.config/gotospaceandbeyond";

	FILE *fptr = fopen(fullpath.c_str(),"a");
	if (fptr == NULL)
	{
		printf("Could not open file");
        return;
	}
	string playerName;
    scanf("%s",playerName.c_str());
    fprintf(fptr,"%d %s\n", score, playerName.c_str());
	fclose(fptr);
    //printf("\e[0m\e[5m\e[4m\e[1m\e[96mpress ESC to exit");
}
void drawFrame(){
    for(int i=0;i<57;i++){
        for(int j=0;j<237;j++){
            //if(i==0||i==56){
               // if(j==0||j==236){
            SetCursorPosition(j,i);
            printf("\e[40m ");

               // }

           // }
        }
    }
}
void showScores() {
    endwin();
    system("sort $HOME/.config/gotospaceandbeyond -nr > $HOME/.config/gotospaceandbeyond.tmp && mv $HOME/.config/gotospaceandbeyond.tmp $HOME/.config/gotospaceandbeyond");
    int line=0,currentLine=0;
    char c;
    FILE *cmd=popen("echo -n $HOME", "r");
    char result[100]={0x0};
    fgets(result, sizeof(result), cmd);
    pclose(cmd);
    string fullpath=result;
    fullpath+="/.config/gotospaceandbeyond";

	FILE *fptr = fopen(fullpath.c_str(),"r");
    if(fptr == NULL) {
        perror("Error in opening file");
        return;
    }
    do {
        c = fgetc(fptr);
        if( feof(fptr) ) {
            break ;
        }
        if(c=='\n'){

            line++;
        }
    } while(1);
    for(int i=0;i<paMaxX;i++){

        for(int j=0;j<paMaxY;j++){

            SetCursorPosition(i,j);
            printf("\033[0m ");
        }
    }
    for(int i=0;i<3;i++) drawFrame();
    if(line==0){
        SetCursorPosition((paMaxX/2)-8,paMaxY/2);
        printf("No scores found.");
        SetCursorPosition(paMaxX-50,paMaxY-2);
        printf("Press Enter to return to main menu.");
        getchar();
        fclose(fptr);

        return;
    }
    rewind(fptr);
    //fclose(fptr);
    string playerName[line];
    string scores[line];
    for(int i=0;i<line;i++){

        playerName[i]="";
        scores[i]="";
    }
    //*fptr = *fopen(fullpath.c_str(),"r");
    if(fptr == NULL) {
        perror("Error in opening file");
        return;
    }
    bool scoreName=0;
    while(currentLine!=line){
        c=fgetc(fptr);

        if(scoreName==1){

            playerName[currentLine]+=c;
        }
        if(scoreName==0){

            scores[currentLine]+=c;
        }
        if(c==' ') scoreName=1;
        if(c=='\n'){
            scoreName=0;
            currentLine++;
        }
    }

    fclose(fptr);

    for(int i=0;i<3;i++) drawFrame();

    currentLine=0;
    bool showLinesIndicator=1;
    if(line<=20) showLinesIndicator=0;
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    //int j=0;

    while(1){
        SetCursorPosition(50,8);
        printf("\033[1;4;92mName");
        SetCursorPosition(180,8);
        printf("Score\033[0m");
        usleep(10000);
        c = getch();
        if(showLinesIndicator==1){

            if(c=='w'){
                for(int i=1;i<paMaxX-1;i++){

                    for(int j=4;j<paMaxY-1;j++){

                        SetCursorPosition(i,j);
                        printf("\033[0m ");
                    }
                }
                if(currentLine!=0) currentLine--;
            }
            if(c=='s'){
                for(int i=1;i<paMaxX-1;i++){

                    for(int j=4;j<paMaxY-1;j++){

                        SetCursorPosition(i,j);
                        printf("\033[0m ");
                    }
                }
                if(currentLine+20!=line) currentLine++;
            }
            for(int i=currentLine;i<20+currentLine;i++){
                SetCursorPosition(45,12+((i-currentLine)*2));
                printf("%d.",i+1);
                SetCursorPosition(50,12+((i-currentLine)*2));
                printf("%s", playerName[i].c_str());
                SetCursorPosition(180,12+((i-currentLine)*2));
                printf("%s", scores[i].c_str());
            }
            SetCursorPosition(10,paMaxY-2);
            printf("lines %d-%d",currentLine+1,currentLine+20);
        }
        if(showLinesIndicator==0){
            for(int i=currentLine;i<line;i++){
                SetCursorPosition(50,12+(i*2));
                printf("%s", playerName[i].c_str());
                SetCursorPosition(180,12+(i*2));
                printf("%s", scores[i].c_str());
            }
        }
        SetCursorPosition(paMaxX-50,paMaxY-6);
        printf("Press W to Scroll Up");
        SetCursorPosition(paMaxX-50,paMaxY-4);
        printf("Press S to Scroll Down");
        SetCursorPosition(paMaxX-50,paMaxY-2);
        printf("Press Enter to return to main menu.");

        if(c=='\n'){

            break;
        }
    }



}

//SetCursorPosition(0,29);for(int i=0;i<10;i++){printf("%d:%d %d |",i+1,bombX[i],bombY[i]);}

